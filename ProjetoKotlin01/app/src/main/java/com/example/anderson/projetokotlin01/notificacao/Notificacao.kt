package com.example.anderson.projetokotlin01.notificacao

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v4.app.NotificationCompat
import com.example.anderson.projetokotlin01.R


object Notificacao {
    fun create(context: Context, id: Int, intent: Intent, titleContent: String, txtContent: String) {
        //Serviço de notificacao do sistema
        val serviceNotify = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        //Validação de versao
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importantLevel = NotificationManager.IMPORTANCE_HIGH
            //Canal
            val notificationCh = NotificationChannel("id", "nameChannel", importantLevel)
            serviceNotify.createNotificationChannel(notificationCh)
        }

        val pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        //montando notificacao
        val builder = NotificationCompat.Builder(context, "id")
                .setContentIntent(pendingIntent)
                .setContentTitle(titleContent)
                .setContentText(txtContent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)

        //finalizando montagem da notificacao
        val notification = builder.build()
        //Iniciando
        serviceNotify.notify(id, notification)

    }
}
