package com.example.anderson.projetokotlin01.adapter

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.anderson.projetokotlin01.R
import com.example.anderson.projetokotlin01.basica.Pokemon
import  kotlinx.android.synthetic.main.item_lista_pokemon.*

/**
 * Created by Anderson,Halyson e Weidson on 08/06/2018.
 */

class AdapterListarPokemons(var ctx: Context, var mListaPokemon: ArrayList<Pokemon>) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

        var view: View
        var viewHolder: ViewHolder

        if (convertView == null) {
            var layout = LayoutInflater.from(ctx)
            view = layout.inflate(R.layout.item_lista_pokemon, parent, false)
            viewHolder = ViewHolder(view)
            view.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }

        var pokemon: Pokemon = getItem(position) as Pokemon

        viewHolder.txtNomePokemon.text = pokemon.nome
        viewHolder.txtTipoPokemon.text = pokemon.tipo
        viewHolder.imageViewPokemon.setImageResource(pokemon.idImagePokemon)

        return view


    }

    override fun getItem(position: Int): Any {

        return mListaPokemon.get(position)
    }

    override fun getItemId(position: Int): Long {

        return position.toLong()

    }

    override fun getCount(): Int {

        return mListaPokemon.size
    }


    private class ViewHolder(var view: View) {
        var txtNomePokemon: TextView
        var txtTipoPokemon: TextView
        var imageViewPokemon: ImageView

        init {
            this.imageViewPokemon = view.findViewById(R.id.imageViewPokemon)
            this.txtNomePokemon = view.findViewById(R.id.txtNomePokemon)
            this.txtTipoPokemon = view.findViewById(R.id.txtPokemonTipo)
        }

    }


}