package com.example.anderson.projetokotlin01.tela


import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.anderson.projetokotlin01.R
import com.example.anderson.projetokotlin01.adapter.AdapterListarPokemons
import com.example.anderson.projetokotlin01.basica.Pokemon


import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var mListaDePokemons = ArrayList<Pokemon>()
    var pokemon: Pokemon? = null
    val adapter = AdapterListarPokemons(this, mListaDePokemons)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mockDadosPokemon()



        listViewPokemon.adapter = adapter

        listViewPokemon.setOnItemClickListener { parent, view, position, id ->

            val intent = Intent(this, DetalheActivity::class.java)
            pokemon = mListaDePokemons.get(position)
            intent.putExtra("pokemon", pokemon)
            intent.putExtra("position", position)
            startActivityForResult(intent, 1)
        }


    }

    fun mockDadosPokemon() {
        mListaDePokemons.add(Pokemon("?", getString(R.string.eletrico), R.mipmap.ic_pikachu_sombra, "Pikachu", false, "Começa com P", "Termina com U"))
        mListaDePokemons.add(Pokemon("?", getString(R.string.agua), R.mipmap.ic_squirtle_sombra, "Squirtle", false, "Começa com S", "Termina com E"))
        mListaDePokemons.add(Pokemon("?", getString(R.string.fogo), R.mipmap.ic_charmander_sombra, "Charmander", false, "Começa com C", "Termina com R"))
        mListaDePokemons.add(Pokemon("?", getString(R.string.psiquico), R.mipmap.ic_mew_sombra, "Mew", false, "Começa com M", "Termina com W"))
        mListaDePokemons.add(Pokemon("?", getString(R.string.normal), R.mipmap.ic_snorlax_sombra, "Snorlax", false, "Começa com S", "Termina com X"))
        mListaDePokemons.add(Pokemon("?", getString(R.string.agua), R.mipmap.ic_gyarados_sombra, "Gyarados", false, "Começa com G", "Termina com S"))
        mListaDePokemons.add(Pokemon("?",getString(R.string.fantasma), R.mipmap.ic_gengar_sombra, "Gengar", false, "Começa com G", "Termina com R"))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1) {

            if (resultCode == Activity.RESULT_OK) {
                pokemon = data?.getSerializableExtra("objetoPokemon") as Pokemon?
                var posicao = data?.getIntExtra("position", -1)


                mListaDePokemons[posicao!!] = pokemon!!

                adapter.notifyDataSetChanged()

                if (pokemon?.acertou!!)
                    Toast.makeText(this,R.string.msg_sucesso, Toast.LENGTH_SHORT).show()
                else
                    Toast.makeText(this, R.string.msg_erro, Toast.LENGTH_SHORT).show()
            }

        }

    }


}
