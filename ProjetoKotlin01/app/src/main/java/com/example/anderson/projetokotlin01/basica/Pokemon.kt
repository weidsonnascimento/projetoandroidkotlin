package com.example.anderson.projetokotlin01.basica

import java.io.Serializable

/**
 * Created by Anderson,Halyson e Weidson on 08/06/2018.
 */
 data class Pokemon ( var nome : String, var tipo : String, var idImagePokemon: Int , var resposta : String, var acertou : Boolean, var dica1 : String, var dica2 : String): Serializable{

}