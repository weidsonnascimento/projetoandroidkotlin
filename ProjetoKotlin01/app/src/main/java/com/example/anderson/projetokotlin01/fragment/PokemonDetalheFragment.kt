package com.example.anderson.projetokotlin01.fragment

import android.annotation.SuppressLint
import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.anderson.projetokotlin01.R
import com.example.anderson.projetokotlin01.basica.Pokemon
import com.example.anderson.projetokotlin01.servico.IntentServiceDicas
import com.example.anderson.projetokotlin01.tela.DetalheActivity

import kotlinx.android.synthetic.main.fragment_detalhe_pokemon.view.*

/**
 * Created by Anderson,Halyson e Weidson  on 09/06/2018.
 */
class PokemonDetalheFragment : Fragment() {

    //estatico
    companion object {
        var pokemon: Pokemon? = null
    }

    var posicao: Int = -1
    var valorEditText: String = ""


    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {


        val view = inflater!!.inflate(R.layout.fragment_detalhe_pokemon, container, false)
        pokemon = activity.intent.getSerializableExtra("pokemon") as Pokemon
        posicao = activity.intent.getIntExtra("position", -1)


        dicasPokemonAlarme()

//Se o usuario acertou, ou seja, estiver true o sistema ira monstrar apenas textView e Imagem do Pokemon Normal
        if (pokemon?.acertou!!) {
            view!!.btnValidarPokemon.visibility = View.INVISIBLE
            view.editNomePokemon.visibility = View.INVISIBLE
            view.textNomePokemon.textSize = 25F
            //var inicio : String = getString(R.string.inicio_frase)
            view.textNomePokemon.text = "${getString(R.string.inicio_frase)} ${pokemon?.resposta!!}!"
        }
// Metodo responsavel por exibir a imagem na tela de detalhe
        verificarFotoTelaDetalhe(view)

        view.btnValidarPokemon.setOnClickListener {

            valorEditText = view.editNomePokemon.text.toString().toUpperCase()

            if (pokemon?.resposta?.toUpperCase() == valorEditText) {
                pokemon?.acertou = true
                pokemon?.nome = pokemon?.resposta!!
                atualizarIconeListaPokemon()
            }

            val returnIntent = Intent()
            returnIntent.putExtra("objetoPokemon", pokemon)
            returnIntent.putExtra("position", posicao)
            activity.setResult(Activity.RESULT_OK, returnIntent)
            activity.finish()
        }

        return view

    }

    private fun dicasPokemonAlarme() {
        val intentDetalhe = Intent(activity, IntentServiceDicas::class.java)

        //Inicio AlarmManager
        val alarmManager = activity.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val pending = PendingIntent.getService(activity, 0, intentDetalhe, PendingIntent.FLAG_UPDATE_CURRENT)
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 5000, pending)

    }

    fun atualizarIconeListaPokemon() {

        when {
            pokemon?.resposta == "Pikachu" -> pokemon?.idImagePokemon = R.mipmap.ic_pikachu_normal
            pokemon?.resposta == "Squirtle" -> pokemon?.idImagePokemon = R.mipmap.ic_squirtle_normal
            pokemon?.resposta == "Charmander" -> pokemon?.idImagePokemon = R.mipmap.ic_charmander_normal
            pokemon?.resposta == "Mew" -> pokemon?.idImagePokemon = R.mipmap.ic_mew_normal
            pokemon?.resposta == "Snorlax" -> pokemon?.idImagePokemon = R.mipmap.ic_snorlax_normal
            pokemon?.resposta == "Gyarados" -> pokemon?.idImagePokemon = R.mipmap.ic_gyarados_normal
            pokemon?.resposta == "Gengar" -> pokemon?.idImagePokemon = R.mipmap.ic_gengar_normal
        }
    }

    fun verificarFotoTelaDetalhe(view: View) {

        if (pokemon?.resposta == "Pikachu" && !pokemon?.acertou!!) {
            view.imgPokemonDetalhe.setImageResource(R.drawable.pikachu_sombra)
        } else if (pokemon?.resposta == "Pikachu" && pokemon?.acertou!!) {
            view.imgPokemonDetalhe.setImageResource(R.drawable.pikachu_normal)
        } else if (pokemon?.resposta == "Squirtle" && !pokemon?.acertou!!) {
            view.imgPokemonDetalhe.setImageResource(R.drawable.squirtle_sombra)
        } else if (pokemon?.resposta == "Squirtle" && pokemon?.acertou!!) {
            view.imgPokemonDetalhe.setImageResource(R.drawable.squirtle_normal)
        } else if (pokemon?.resposta == "Charmander" && !pokemon?.acertou!!) {
            view.imgPokemonDetalhe.setImageResource(R.drawable.charmander_sombra)
        } else if (pokemon?.resposta == "Charmander" && pokemon?.acertou!!) {
            view.imgPokemonDetalhe.setImageResource(R.drawable.charmander_normal)
        } else if (pokemon?.resposta == "Mew" && !pokemon?.acertou!!) {
            view.imgPokemonDetalhe.setImageResource(R.drawable.mew_sombra)
        } else if (pokemon?.resposta == "Mew" && pokemon?.acertou!!) {
            view.imgPokemonDetalhe.setImageResource(R.drawable.mew_normal)
        } else if (pokemon?.resposta == "Snorlax" && !pokemon?.acertou!!) {
            view.imgPokemonDetalhe.setImageResource(R.drawable.snolax_sombra)
        } else if (pokemon?.resposta == "Snorlax" && pokemon?.acertou!!) {
            view.imgPokemonDetalhe.setImageResource(R.drawable.snorlax_normal)
        } else if (pokemon?.resposta == "Gyarados" && !pokemon?.acertou!!) {
            view.imgPokemonDetalhe.setImageResource(R.drawable.gyarados_sombra)
        } else if (pokemon?.resposta == "Gyarados" && pokemon?.acertou!!) {
            view.imgPokemonDetalhe.setImageResource(R.drawable.gyarados_normal)
        } else if (pokemon?.resposta == "Gengar" && !pokemon?.acertou!!) {
            view.imgPokemonDetalhe.setImageResource(R.drawable.gengar_sombra)
        } else if (pokemon?.resposta == "Gengar" && pokemon?.acertou!!) {
            view.imgPokemonDetalhe.setImageResource(R.drawable.gengar_normal)
        }




    }
}