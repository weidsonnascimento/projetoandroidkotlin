package com.example.anderson.projetokotlin01.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.anderson.projetokotlin01.basica.Pokemon
import com.example.anderson.projetokotlin01.notificacao.Notificacao
import com.example.anderson.projetokotlin01.tela.DetalheActivity

class BroadcastReceiver : BroadcastReceiver() {
    //broadcast de iniciação da notificacao

    var pokemon:Pokemon? = null
    companion object {
        var count = 0
    }

    override fun onReceive(context: Context, intent: Intent) {

        pokemon = intent.getSerializableExtra("pokemonSelecionado") as Pokemon?

        if (pokemon != null){
            val intentDetalhe = Intent(context, DetalheActivity::class.java)
            count++
            if(count == 1){
                Notificacao.create(context, 1, intentDetalhe, "Dica 1:", pokemon?.tipo!!)
            }
            if(count == 2){
                Notificacao.create(context, 1, intentDetalhe, "Dica 2:", pokemon?.dica1!!)
            }
            if(count == 3) {
                Notificacao.create(context, 1, intentDetalhe, "Dica 3:", pokemon?.dica2!!)
                count = 0
            }

        }



    }
}
