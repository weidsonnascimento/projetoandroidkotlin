package com.example.anderson.projetokotlin01.servico

import android.app.IntentService
import android.content.Intent
import android.content.Context
import com.example.anderson.projetokotlin01.basica.Pokemon
import com.example.anderson.projetokotlin01.fragment.PokemonDetalheFragment


class IntentServiceDicas : IntentService("IntentServiceDicas") {

    var pokemon:Pokemon? = null


    override fun onHandleIntent(intent: Intent?) {

        val intentService = Intent("broadcastReceiver")
        intentService.putExtra("pokemonSelecionado", PokemonDetalheFragment.pokemon)

        sendBroadcast(intentService)


    }


}
